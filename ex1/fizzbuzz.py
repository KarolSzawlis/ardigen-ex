def fizzbuzz(n: int, m: int):
    for number in range(n, m + 1):
        if number % 15 == 0:
            print("fizzbuzz")
            continue
        elif number % 5 == 0:
            print("buzz")
            continue
        elif number % 3 == 0:
            print("fizz")
            continue
        print(number)


fizzbuzz(3, 16)
