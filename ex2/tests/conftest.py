import pandas as pd


def invalid_matching_df():
    data = {'matching_id': [1],
            'top_priced_count': [2]
            }
    return pd.DataFrame(data)


def data_df():
    data = {'id': [1, 2, 3, 4, 5, 6, 7, 8, 9],
            'price': [1000, 1050, 2000, 1750, 1400, 7000, 630, 4000, 1400],
            'currency': ['GBP', 'EU', 'PLN', 'EU', 'EU', 'PLN', 'EU', 'GBP', 'EU'],
            'quantity': [2, 1, 1, 2, 4, 3, 5, 1, 3],
            'matching_id': [3, 1, 1, 2, 3, 2, 3, 3, 1]}
    return pd.DataFrame(data)


def currencies_df():
    data = {'currency': ['GBP', 'EU', 'PLN'],
            'ratio': [2.4, 2.1, 1]
            }
    return pd.DataFrame(data)


def matching_df():
    data = {'matching_id': [1, 2, 3], 'top_priced_count': [2, 2, 3]}
    return pd.DataFrame(data)
