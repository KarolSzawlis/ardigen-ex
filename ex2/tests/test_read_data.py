import unittest
from ex2.utils import read_csv, InputFileException
import pandas as pd
from pandas.testing import assert_frame_equal


class TestReadData(unittest.TestCase):

    def test_read_not_supported_filename(self):
        """Test raises exception when file_name argument is not supported by read_csv function"""
        with self.assertRaises(InputFileException):
            read_csv(file_name='not_supported')

    def test_read_filename_with_format(self):
        """Test raises exception when file_name has csv format in name"""
        with self.assertRaises(InputFileException):
            read_csv(file_name='matchings.csv')

    def test_raises_exception_when_calling_is_not_correct(self):
        "Test raises exception when file_name is not key word argument"
        with self.assertRaises(InputFileException):
            read_csv('matchings.csv')

    def test_read_correct_data_filename(self):
        """Test correct read data file with proper naming"""
        data = {'id': [1],
                'price': [1000],
                'currency': ['GBP'],
                'quantity': [2],
                'matching_id': [3]}
        expected_df = pd.DataFrame(data)
        actuall_df = read_csv(file_name='data')
        assert_frame_equal(expected_df, actuall_df, check_dtype=True)

    def test_read_correct_matching_filename(self):
        """Test correct read matching file with proper naming"""
        data = {'matching_id': [1],
                'top_priced_count': [1]
                }
        expected_df = pd.DataFrame(data)
        actuall_df = read_csv(file_name='matchings')
        assert_frame_equal(expected_df, actuall_df, check_dtype=True)

    def test_read_correct_currencies_filename(self):
        """Test correct read currencies file with proper naming"""
        data = {'currency': ['GBP', 'EU', 'PLN'],
                'ratio': [2.4, 2.1, 1]
                }
        expected_df = pd.DataFrame(data)
        actuall_df = read_csv(file_name='currencies')
        assert_frame_equal(expected_df, actuall_df, check_dtype=True)



if __name__ == '__main__':
    unittest.main()
