import unittest

from assertpy import assert_that

from ex2.tests.conftest import data_df, invalid_matching_df, currencies_df, matching_df
from ex2.valuation import Valuation, TopPriceCountException
import pandas as pd


class TestValuation(unittest.TestCase):
    DATA_DF = data_df()
    INVALID_MATCHING_DF = invalid_matching_df()
    CURRENCIES_DF = currencies_df()
    MATCHING_DF = matching_df()

    def test_invalid_top_price_count(self):
        """Check if vaulation raise exception when top price has incorrect value"""
        with self.assertRaises(TopPriceCountException):
            v = Valuation(self.DATA_DF, self.CURRENCIES_DF, self.INVALID_MATCHING_DF)
            v.valuate()

    def test_full_valuation_flow(self):
        """Test full postive valuation flow"""
        v = Valuation(self.DATA_DF, self.CURRENCIES_DF, self.MATCHING_DF)
        actuall_df = v.valuate()
        data = {'matching_id': [1, 2, 3],
                'total_price': [11025.0, 28350.0, 27975.0],
                'avg_price': [5512.5, 14175.0, 9325.0],
                'currency': ['PLN', 'PLN', 'PLN'],
                'ignored_products_count': [1, 0, 1]}
        expected_df = pd.DataFrame(data)
        assert_that(actuall_df.values.tolist()).is_equal_to(expected_df.values.tolist())


if __name__ == '__main__':
    unittest.main()
