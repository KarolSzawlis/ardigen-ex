from functools import wraps
import pandas as pd
from enum import Enum


class InputFileException(Exception):
    pass


class FileName(Enum):
    data = 'data'
    currencies = 'currencies'
    matchings = 'matchings'


def file_name_validator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        file_name = kwargs.get('file_name')
        if file_name not in [e.value for e in FileName]:
            raise InputFileException(f'File with name {file_name} is not supported by the system')
        return func(*args, **kwargs)

    return wrapper


@file_name_validator
def read_csv(file_name: str) -> pd.DataFrame:
    df = pd.read_csv(f'{file_name}.csv')
    return df


def write_csv(df: pd.DataFrame):
    df.to_csv('top_products.csv', sep=',', index=False)
