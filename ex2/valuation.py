from enum import Enum
import pandas as pd
from pandas import DataFrame


class Currency(Enum):
    PLN = 'PLN'
    GBP = 'GBP'
    EU = 'EU'


class TopPriceCountException(Exception):
    pass


class Valuation:
    def __init__(self, currencies_df: DataFrame, data_df: DataFrame, matchings_df: DataFrame):
        self._currencies_df = currencies_df
        self._data_df = data_df
        self._matchings_df = matchings_df
        self._base_df = None

    def _create_base_df(self):
        base_df = pd.merge(self._data_df, self._currencies_df, on='currency')
        for index, row in base_df.iterrows():
            pl = row['price'] * row['quantity'] * row['ratio']
            base_df.at[index, 'pln_value'] = pl
        self._base_df = base_df

    @staticmethod
    def _get_matching_id(row) -> str:
        return row['matching_id']

    @staticmethod
    def _get_PLN_currency() -> str:
        return Currency.PLN.value

    @staticmethod
    def _get_top_priced_count(row) -> int:
        return row['top_priced_count']

    @staticmethod
    def _get_ignored_product_count(matching_id_df: DataFrame, row) -> int:
        ignored_products_count = matching_id_df.shape[0] - row['top_priced_count']
        if ignored_products_count < 0:
            raise TopPriceCountException(
                f'Can not count f{row["top_priced_count"]} because there is only {matching_id_df.shape[0]}')
        return ignored_products_count

    @staticmethod
    def _get_top_priced_df(matching_id_df: DataFrame, top_priced_count):
        return matching_id_df.sort_values(by=['pln_value'], ascending=False).head(top_priced_count)

    @staticmethod
    def _get_total_price(top_priced_df: DataFrame):
        return top_priced_df['pln_value'].sum()

    @staticmethod
    def _get_mean_price(top_priced_df: DataFrame):
        return top_priced_df['pln_value'].mean()

    def valuate(self) -> DataFrame:
        if not self._base_df:
            self._create_base_df()
        result_df = pd.DataFrame(
            columns=['matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count'])
        for index, row in self._matchings_df.iterrows():
            matching_id = self._get_matching_id(row)
            matching_id_df = self._base_df.loc[self._base_df['matching_id'] == matching_id]
            ignored_products_count = self._get_ignored_product_count(matching_id_df, row)
            top_pirced_count = self._get_top_priced_count(row)
            top_priced_df = self._get_top_priced_df(matching_id_df, top_pirced_count)
            result_row = {
                'matching_id': matching_id,
                'total_price': self._get_total_price(top_priced_df),
                'avg_price': self._get_mean_price(top_priced_df),
                'currency': self._get_PLN_currency(),
                'ignored_products_count': ignored_products_count
            }
            result_df = result_df.append(result_row, ignore_index=True)
        return result_df
