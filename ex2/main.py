from ex2.valuation import Valuation

from ex2.utils import read_csv, write_csv


def main():
    currencies_df = read_csv(file_name='currencies')
    data_df = read_csv(file_name='data')
    matchings_df = read_csv(file_name='matchings')
    valuation = Valuation(currencies_df, data_df, matchings_df)
    result_df = valuation.valuate()
    write_csv(result_df)


if __name__ == '__main__':
    main()
